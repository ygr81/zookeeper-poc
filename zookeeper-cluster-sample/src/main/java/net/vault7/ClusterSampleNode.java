package net.vault7;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Supplier;

class ClusterSampleNode {
    private static final Logger logger = LoggerFactory.getLogger(ClusterSampleNode.class);

    private final String persistentNode;
    private final String persistentData;
    private final ZooKeeperFacade zooKeeperFacade;

    ClusterSampleNode(final Supplier<ZooKeeperFacade> supplier, final String persistentNode, final String persistentData) {
        this.persistentNode = persistentNode.startsWith("/") ? persistentNode : "/" + persistentNode;
        this.persistentData = persistentData;
        this.zooKeeperFacade = supplier.get();
    }

    void create() {
        logger.info("Create node '{}' with data '{}'", persistentNode, persistentData);
        zooKeeperFacade.createPersistent(persistentNode, persistentData);
    }

    void removeIfExists() {
        logger.info("Remove node '{}'", persistentNode);
        zooKeeperFacade.remover(persistentNode);
    }

    void close() {
        logger.info("Closing connection...");
        zooKeeperFacade.close();
    }
}
