package net.vault7;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Supplier;

public class ClusterSample {
    private static final Logger logger = LoggerFactory.getLogger(ClusterSample.class);

    private static final String PATH = "/cluster-sample";

    public static void main(String[] args) {
        logger.info("Cluster sample start...");
        List<Supplier<ZooKeeperFacade>> suppliers = new ArrayList<>();

        for (String arg : args) {
            suppliers.add(() -> ZooKeeperFacade.connect(arg));
        }

        final ClusterSampleNode clusterSampleNode = new ClusterSampleNode(suppliers.get(0),
                PATH, "cluster-sample-data");

        clusterSampleNode.removeIfExists();

        clusterSampleNode.create();

        suppliers.
                forEach((supplier) -> {
                            final int value = nextInt();
                            startWorker(new WriterWorker(supplier, PATH, "writer-" + value), 1000, 2000);
                            startWorker(new ReaderWorker(supplier, PATH, "reader-" + value), 10);
                        }
                );

        clusterSampleNode.close();
    }

    private static int nextInt = 1;

    private static int nextInt() {
        return nextInt++;
    }

    private static void startWorker(final Worker worker, final int minInterval, final int maxInterval) {
        startWorker(worker, ThreadLocalRandom.current().nextInt(minInterval, maxInterval + 1));
    }

    private static void startWorker(final Worker worker, final int interval) {
        new Thread(() -> {
            logger.info("Starting worker {}", worker);
            while (true) {
                if (Thread.interrupted()) {
                    break;
                }
                worker.doJob();
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException e) {
                    break;
                }
            }
        }).start();
    }
}
