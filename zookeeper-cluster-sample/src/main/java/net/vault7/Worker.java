package net.vault7;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Supplier;
import java.util.stream.Collectors;

interface Worker {
    void doJob();
}

class ReaderWorker implements Worker {

    private static final Logger logger = LoggerFactory.getLogger(ReaderWorker.class);

    private final String rootPath;
    private final String workerName;
    private final ZooKeeperFacade zooKeeperFacade;

    ReaderWorker(final Supplier<ZooKeeperFacade> supplier,
                 final String rootPath,
                 final String workerName) {
        this.workerName = workerName;
        this.rootPath = rootPath;
        this.zooKeeperFacade = supplier.get();
    }

    @Override
    public void doJob() {
        try {
            read(rootPath);
        } catch (ZooKeeperException e) {
            logger.error(e.getMessage());
        }
    }

    private void read(final String path) {
        String read = zooKeeperFacade.getChildren(path).stream().
                map((child) -> child + ": " + getDataSafely(path + "/" + child)).
                collect(Collectors.joining(","));
        logger.info("{}: {}", workerName, read);
    }

    private String getDataSafely(final String path) {
        try {
            return zooKeeperFacade.getData(path);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return "";
        }
    }

    @Override
    public String toString() {
        return "ReaderWorker{" +
                "workerName='" + workerName + '\'' +
                '}';
    }
}

class WriterWorker implements Worker {
    private static final Logger logger = LoggerFactory.getLogger(WriterWorker.class);

    private final String workerName;
    private final String workerPath;
    private final String workerEphemeralPath;
    private final ZooKeeperFacade zooKeeperFacade;

    WriterWorker(final Supplier<ZooKeeperFacade> supplier,
                 final String rootPath,
                 final String workerName) {
        this.workerName = workerName;
        this.zooKeeperFacade = supplier.get();
        workerPath = rootPath + "/" + workerName;
        workerEphemeralPath = rootPath + "/" + workerName + "-ephemeral";
    }

    @Override
    public void doJob() {
        try {
            if (!zooKeeperFacade.exists(workerPath)) {
                logger.info("{}: Create {}", workerName, workerPath);
                zooKeeperFacade.createPersistent(workerPath, "0");
            } else {
                logger.info("{}: Update {}", workerName, workerPath);
                update(workerPath);
            }

            if (!zooKeeperFacade.exists(workerEphemeralPath)) {
                logger.info("{}: Create {}", workerName, workerEphemeralPath);
                zooKeeperFacade.createEphemeral(workerEphemeralPath, "0");
            } else {
                logger.info("{}: Update {}", workerName, workerEphemeralPath);
                update(workerEphemeralPath);
            }
        } catch (ZooKeeperException e) {
            logger.error(e.getMessage());
        }
    }

    private void update(final String path) {
        String value = zooKeeperFacade.getData(path);
        logger.info("{}: Old value for path {} is {}", workerName, path, value);
        int v = Integer.parseInt(value);
        zooKeeperFacade.setData(path, Integer.toString(v + 1));
    }

    @Override
    public String toString() {
        return "Worker{" +
                "workerName='" + workerName + '\'' +
                '}';
    }
}
