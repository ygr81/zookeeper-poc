package net.vault7;

import com.google.common.base.Preconditions;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.RetryNTimes;
import org.apache.zookeeper.CreateMode;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;

class ZooKeeperException extends RuntimeException {
    ZooKeeperException(Throwable t) {
        super(t);
    }
}

class ZooKeeperFacade {
    private final CuratorFramework curatorFramework;

    private ZooKeeperFacade(final CuratorFramework curatorFramework) {
        this.curatorFramework = curatorFramework;
    }

    void close() {
        curatorFramework.close();
    }

    String createPersistent(final String path, final String data) {
        Preconditions.checkArgument(path.startsWith("/"));
        try {
            return curatorFramework.create().creatingParentsIfNeeded().forPath(path, getBytes(data));
        } catch (Exception e) {
            throw new ZooKeeperException(e);
        }
    }

    boolean exists(final String path) {
        try {
            return curatorFramework.checkExists().forPath(path) != null;
        } catch (Exception e) {
            return false;
        }
    }

    String getData(final String path) {
        try {
            byte[] bytes = curatorFramework.getData().forPath(path);
            return new String(bytes, StandardCharsets.UTF_8);
        } catch (Exception e) {
            throw new ZooKeeperException(e);
        }
    }

    void setData(final String path, final String data) {
        try {
            curatorFramework.setData().forPath(path, getBytes(data));
        } catch (Exception e) {
            throw new ZooKeeperException(e);
        }
    }

    String createEphemeral(final String path, final String data) {
        try {
            return curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL).forPath(path, getBytes(data));
        } catch (Exception e) {
            throw new ZooKeeperException(e);
        }
    }

    void remove(final String path) {
        try {
            curatorFramework.delete().deletingChildrenIfNeeded().forPath(path);
        } catch (Exception e) {
            throw new ZooKeeperException(e);
        }
    }

    void remover(final String path) {
        remove(path);
    }

    private byte[] getBytes(final String data) {
        if (data == null) {
            return null;
        }
        try {
            return data.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            return data.getBytes();
        }
    }

    List<String> getChildren(final String path) {
        try {
            return curatorFramework.getChildren().forPath(path);
        } catch (Exception e) {
            throw new ZooKeeperException(e);
        }
    }

    static ZooKeeperFacade connect(final String ensembleHosts) {
        CuratorFramework cf = CuratorFrameworkFactory.newClient(ensembleHosts, new RetryNTimes(5, 1000));
        cf.start();
        return new ZooKeeperFacade(cf);
    }
}
