package net.vault7;

import org.apache.zookeeper.KeeperException;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.function.Supplier;

public class SimpleAPI {
    public static void main(String[] args) {
        final CountDownLatch tearDownLatch = new CountDownLatch(3);
        final CountDownLatch createdLatch = new CountDownLatch(1);

        Supplier<ZooKeeperFacade> zooKeeperFacadeSupplier =
                () -> ZooKeeperFacade.connect(args.length > 0 ? args[0] : "192.168.71.11:2181");

        final PersistentNode persistentNode = new PersistentNode(zooKeeperFacadeSupplier,
                PERSISTENT_NODE, PERSISTENT_NODE);
        final EphemeralNode ephemeralNode = new EphemeralNode(zooKeeperFacadeSupplier, PERSISTENT_NODE);
        final ListNodes listNodes = new ListNodes(zooKeeperFacadeSupplier, PERSISTENT_NODE);

        // create persistent node
        new Thread(() -> {
            try {
                persistentNode.create();
            } catch (InterruptedException | KeeperException e) {
                e.printStackTrace();
            } finally {
                createdLatch.countDown();
                tearDownLatch.countDown();
            }
        }).start();

        // create ephemerals
        new Thread(() -> {
            try {
                createdLatch.await();
                ephemeralNode.create(EPHEMERAL_NODE_1);
                Thread.sleep(1000);
                ephemeralNode.create(EPHEMERAL_NODE_2);
                Thread.sleep(1000);
                ephemeralNode.create(EPHEMERAL_NODE_3);
            } catch (InterruptedException | KeeperException e) {
                e.printStackTrace();
            } finally {
                tearDownLatch.countDown();
            }
        }).start();

        // list threads
        new Thread(() -> {
            try {
                createdLatch.await();
                while (true) {
                    if (Thread.interrupted()) {
                        break;
                    }
                    List<String> children = listNodes.list();
                    for (String child : children) {
                        System.out.println(child);
                    }
                    if (children.size() == 3) {
                        break;
                    }
                    System.out.println("");
                }
            } catch (InterruptedException | KeeperException e) {
                e.printStackTrace();
            } finally {
                tearDownLatch.countDown();
            }
        }).start();

        // remove persistent node
        new Thread(() -> {
            try {
                tearDownLatch.await();
                listNodes.close();
                ephemeralNode.remove();
                persistentNode.remove();
                persistentNode.close();
            } catch (InterruptedException | KeeperException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private static final String PERSISTENT_NODE = "sample-persistent-node";
    private static final String EPHEMERAL_NODE_1 = "sample-ephemeral-node-1";
    private static final String EPHEMERAL_NODE_2 = "sample-ephemeral-node-2";
    private static final String EPHEMERAL_NODE_3 = "sample-ephemeral-node-3";
}
