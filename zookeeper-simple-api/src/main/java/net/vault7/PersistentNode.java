package net.vault7;

import org.apache.zookeeper.KeeperException;

import java.util.function.Supplier;

class PersistentNode {
    private final String persistentNode;
    private final String persistentData;
    private final ZooKeeperFacade zooKeeperFacade;

    PersistentNode(final Supplier<ZooKeeperFacade> supplier, final String persistentNode, final String persistentData) {
        this.persistentNode = persistentNode.startsWith("/") ? persistentNode : "/" + persistentNode;
        this.persistentData = persistentData;
        this.zooKeeperFacade = supplier.get();
    }

    void create()
            throws InterruptedException, KeeperException {
        zooKeeperFacade.createPersistent(persistentNode, persistentData);
    }

    void remove() throws InterruptedException, KeeperException {
        zooKeeperFacade.remover(persistentNode);
    }

    void close() throws InterruptedException, KeeperException {
        zooKeeperFacade.close();
    }
}
