package net.vault7;

import org.apache.zookeeper.KeeperException;

import java.util.function.Supplier;

class EphemeralNode {

    private final String persistentNode;

    EphemeralNode(final Supplier<ZooKeeperFacade> supplier, final String persistentNode) {
        this.persistentNode = persistentNode.startsWith("/") ? persistentNode : "/" + persistentNode;
        zooKeeperFacade = supplier.get();
    }

    private final ZooKeeperFacade zooKeeperFacade;

    void create(final String ephemeralNode) throws InterruptedException, KeeperException {
        final String userNode = ephemeralNode.startsWith("/") ? ephemeralNode : "/" + ephemeralNode;
        zooKeeperFacade.createEphemeral(persistentNode + userNode, null);
    }

    void remove() throws InterruptedException, KeeperException {
        zooKeeperFacade.close();
    }
}
