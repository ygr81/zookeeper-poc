package net.vault7;

import org.apache.zookeeper.KeeperException;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.function.Supplier;

class ListNodes {

    private final String persistentNode;

    ListNodes(final Supplier<ZooKeeperFacade> supplier, final String persistentNode) {
        this.persistentNode = persistentNode.startsWith("/") ? persistentNode : "/" + persistentNode;
        zooKeeperFacade = supplier.get();
    }

    private final ZooKeeperFacade zooKeeperFacade;

    private final Semaphore semaphore = new Semaphore(1);

    List<String> list() throws InterruptedException, KeeperException {
        semaphore.acquire();
        List<String> children = zooKeeperFacade.getChildren(persistentNode, event -> semaphore.release());

        Collections.sort(children);
        return children;
    }

    void close() throws InterruptedException, KeeperException {
        zooKeeperFacade.close();
    }
}
