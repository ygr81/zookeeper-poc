package net.vault7;

import com.google.common.base.Preconditions;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

class ZooKeeperFacade {

    private final ZooKeeper zk;

    private ZooKeeperFacade(final ZooKeeper zk) {
        this.zk = zk;
    }

    void close() throws InterruptedException {
        zk.close();
    }

    String createPersistent(final String path, final String data) {
        try {
            return createPersistentUnsafe(path, data);
        } catch (KeeperException | InterruptedException e) {
            throw new ZooKeeperException(e);
        }
    }

    private String createPersistentUnsafe(final String path, final String data) throws KeeperException, InterruptedException {
        Preconditions.checkArgument(path.startsWith("/"));
        if (zk.exists(path, false) != null) {
            return path;
        }
        return createNode(path, data, CreateMode.PERSISTENT);
    }

    boolean exists(final String path) {
        try {
            return zk.exists(path, false) != null;
        } catch (KeeperException | InterruptedException e) {
            return false;
        }
    }

    String getData(final String path) {
        try {
            return getDataUnsafe(path);
        } catch (KeeperException | InterruptedException e) {
            throw new ZooKeeperException(e);
        }
    }

    private String getDataUnsafe(final String path) throws KeeperException, InterruptedException {
        byte[] bytes = zk.getData(path, false, null);
        return new String(bytes, StandardCharsets.UTF_8);
    }

    void setData(final String path, final String data) {
        try {
            setDataUnsafe(path, data);
        } catch (KeeperException | InterruptedException e) {
            throw new ZooKeeperException(e);
        }
    }

    private void setDataUnsafe(final String path, final String data) throws KeeperException, InterruptedException {
        zk.setData(path, getBytes(data), -1);
    }

    String createEphemeral(final String path, final String data) {
        try {
            return createEphemeralUnsafe(path, data);
        } catch (KeeperException | InterruptedException e) {
            throw new ZooKeeperException(e);
        }
    }

    private String createEphemeralUnsafe(final String path, final String data) throws KeeperException, InterruptedException {
        Preconditions.checkArgument(path.startsWith("/"));
        return createNode(path, data, CreateMode.EPHEMERAL);
    }

    void remove(final String path) {
        try {
            removeUnsafe(path);
        } catch (KeeperException | InterruptedException e) {
            throw new ZooKeeperException(e);
        }
    }

    private void removeUnsafe(final String path) throws KeeperException, InterruptedException {
        Preconditions.checkArgument(path.startsWith("/"));
        zk.delete(path, -1);
    }

    void remover(final String path) {
        try {
            removerUnsafe(path);
        } catch (KeeperException | InterruptedException e) {
            throw new ZooKeeperException(e);
        }
    }

    private void removerUnsafe(final String path) throws KeeperException, InterruptedException {
        Preconditions.checkArgument(path.startsWith("/"));
        List<String> children = zk.getChildren(path, false);
        for (String child : children) {
            remover(path + "/" + child);
        }
        remove(path);
    }

    private String createNode(final String path, final String data, final CreateMode createMode)
            throws KeeperException, InterruptedException {
        Preconditions.checkArgument(path.startsWith("/"));
        return zk.create(path,
                getBytes(data),
                ZooDefs.Ids.OPEN_ACL_UNSAFE,
                createMode);
    }

    private byte[] getBytes(final String data) {
        if (data == null) {
            return null;
        }
        try {
            return data.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            return data.getBytes();
        }
    }

    List<String> getChildren(final String path) {
        try {
            return getChildren(path, event -> System.out.println(event.getType()));
        } catch (InterruptedException e) {
            throw new ZooKeeperException(e);
        }
    }

    List<String> getChildren(final String path, final Watcher watcher) throws InterruptedException {
        try {
            return getChildrenUnsafe(path, watcher);
        } catch (KeeperException e) {
            throw new ZooKeeperException(e);
        }
    }

    private List<String> getChildrenUnsafe(final String path, final Watcher watcher) throws KeeperException, InterruptedException {
        return zk.getChildren(path, watcher);
    }

    private static ZooKeeperFacade createConnection(final String ensembleHosts)
            throws IOException, InterruptedException {
        final CountDownLatch connectionLatch = new CountDownLatch(1);
        final int sessionTimeout = 30;
        ZooKeeper zk = new ZooKeeper(ensembleHosts, sessionTimeout, event -> {
            if (Watcher.Event.KeeperState.SyncConnected.equals(event.getState())) {
                connectionLatch.countDown();
            }
        });
        if (!connectionLatch.await(5, TimeUnit.SECONDS)) {
            throw new ZooKeeperConnectionFailure();
        }
        return new ZooKeeperFacade(zk);
    }

    static ZooKeeperFacade connect(final String ensembleHosts) {
        try {
            return createConnection(ensembleHosts);
        } catch (IOException | InterruptedException e) {
            throw new ZooKeeperConnectionFailure();
        }
    }
}
