package net.vault7;

class ZooKeeperException extends RuntimeException {
    public ZooKeeperException(Throwable cause) {
        super(cause);
    }
}
