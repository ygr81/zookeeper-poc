package net.vault7;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class DepartmentService {
    private static final String DEPARTMENT_PATH = "/department";

    @Value("${departmentName}")
    private String departmentName;

    @RequestMapping(path = DEPARTMENT_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
    public Department getDescription() {
        return new Department(departmentName);
    }
}
