package net.vault7;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

class DepartmentSerializer extends JsonSerializer<Department> {
    @Override
    public void serialize(Department value, JsonGenerator gen, SerializerProvider serializers)
            throws IOException {
        gen.writeStartObject();
        gen.writeStringField("name", value.name());
        gen.writeEndObject();
    }
}
