package net.vault7;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = DepartmentSerializer.class)
class Department {
    private final String name;

    Department(final String name) {
        this.name = name;
    }

    String name() {
        return name;
    }
}
