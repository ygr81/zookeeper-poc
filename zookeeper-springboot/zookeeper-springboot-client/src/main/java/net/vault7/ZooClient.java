package net.vault7;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Configuration
@EnableFeignClients
@EnableDiscoveryClient
class ZooClient {
    @Autowired
    private TheClient theClient;

    @FeignClient(name = "zoo-department")
    interface TheClient {
        @RequestMapping(value = "/department", consumes = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        Department getDepartment();
    }

    Department getDepartment() {
        return theClient.getDepartment();
    }

}
